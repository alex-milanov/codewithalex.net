# CodewithAlex.net
- the site where I've setup my livecoding steam
- also there are videos from previous streams

## install
```sh
npm i
```

## env file
- create an .env file with the following vars:
```sh
# local
VIDEOS_PATH=./videos/
# streaming
STRM_URL=http://streamurl/stream.m3u8
STRM_SERVER=streamserver.net
STRM_DUMP_PATH=/path/to/video/dumps
# video on demand
VOD_SERVER=vodserver.net
VOD_VIDEOS_PATH=/path/to/vod/videos/
VOD_APP_PATH=/path/to/vod/app
```

## build
```sh
npm run build
```

## run locally
- open http://localhost:3000
```sh
npm start
```
