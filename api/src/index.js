
const express = require('express');
const bodyParser = require('body-parser');
const proxy = require('express-http-proxy');
const cors = require('cors');
const morgan = require('morgan');

const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());
app.use(morgan('dev'));

//
app.use('/api/status', proxy('stream:8080', {
	proxyReqPathResolver: req => (console.log(req.url), `/api/status`)
}));


// connect
const port = process.env.PORT ?? 80;
app.listen(port, () => console.log(`api listening to port ${port}`));
