'use strict';

const {
	section, button, span,
	footer, ul, li, img, i,
	h1, a, h
} = require('iblokz-snabbdom-helpers');

const lbrySvg = () => h('svg.icon.lbry-icon', {
	attrs: {
		stroke: "currentColor",
		fill: "currentColor",
		x: "0px", y: "0px", viewBox: "0 0 322 254"
	}
}, [
	h('path', {attrs: {
		d: "M296,85.9V100l-138.8,85.3L52.6,134l0.2-7.9l104,51.2L289,96.1v-5.8L164.2,30.1L25,116.2v38.5l131.8,65.2 l137.6-84.4l3.9,6l-141.1,86.4L18.1,159.1v-46.8l145.8-90.2C163.9,22.1,296,85.9,296,85.9z"
	}}),
	h('path', {attrs: {
		d: "M294.3,150.9l2-12.6l-12.2-2.1l0.8-4.9l17.1,2.9l-2.8,17.5L294.3,150.9L294.3,150.9z"
	}})
]);

module.exports = ({state, actions}) => footer([
	ul(`.left`, [
		li(a({attrs: {
			href: "https://lbry.tv/@codewithalex:e",
			target: "_blank", title: 'Check out my channel at lbry.tv'}}, lbrySvg())),
		li(a({attrs: {
			href: 'https://www.youtube.com/channel/UCtyQdTEQycrinGXj6LpgfPw',
			target: "_blank", title: 'Check out my YouTube channel'
		}}, i('.fa.fa-youtube'))),
		li(a({attrs: {
			href: 'https://twitter.com/alex_milanov_',
			target: "_blank", title: 'Follow me on twitter'
		}}, i('.fa.fa-twitter'))),
		li(a({attrs: {
			href: 'https://github.com/alex-milanov',
			target: "_blank", title: 'Check out my repos at github'
		}}, i('.fa.fa-github'))),
		li(a({attrs: {
			href: 'https://gitlab.com/alex-milanov',
			target: "_blank", title: 'Check out my repos at gitlab'
		}}, i('.fa.fa-gitlab'))),
		li(`<- follow me here`)
	])
]);
