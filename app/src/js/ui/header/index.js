'use strict';

const {
	section, button, span,
	header, ul, li, img, i,
	h1, a
} = require('iblokz-snabbdom-helpers');

module.exports = ({state, actions}) => header([
	ul(`.left`, [
		li(a(`.fa.fa-television[title="watch livestream"]`, {
			class: {active: state.videoId === -1},
			on: {click: () => actions.changeVideo(-1)}
		})),
		li(a(`.fa.fa-film`, {
			class: {active: state.offline || state.video},
			on: {click: () => actions.toggle('video')}}, [])),
		li(`<- previous streams`)
	]),
	h1(['Code with Alex', span({
		style: {
			color: state.offline ? `#eee` : `#4f6`
		}
	}, ` [${state.offline ? 'offline' : 'live'}]`)]),
	ul(`.right`, [
		li(state.chat ? '' : `click to toggle chat ->`),
		li(a(`.fa.fa-comments-o`, {
			class: {active: state.chat},
			on: {click: () => actions.toggle('chat')}
		}))
	])
]);
