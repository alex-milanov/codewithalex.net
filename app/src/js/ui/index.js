'use strict';

// dom
const {
	h1, a, div, iframe,
	section, button, span,
	ul, li, figure, figcaption, img
} = require('iblokz-snabbdom-helpers');
// components
const header = require('./header');
const footer = require('./footer');
const video = require('./video');

module.exports = ({state, actions}) => section('#ui', [
	header({state, actions}),
	section('.stream', [
		ul('.videos', {
			style: {
				width: state.offline || state.video ? `240px` : `0px`,
				overflowY: 'scroll'
			}
		}, state.videos.sort((a, b) => a.dateTime > b.dateTime ? -1 : 1)
			.map((vid, videoId) => li(figure({
				on: {click: () => actions.changeVideo(videoId)}
			}, [
				img(`[src="${vid.thumb}"]`),
				figcaption(vid.title)
			]))
		)),
		video({state, actions}),
		iframe(`.chat`, {
			style: {
				width: state.chat ? `420px` : `0px`
			},
			attrs: {
				src: `https://webirc.ludost.net/?randomnick=1&channels=codewithalex&uio=MTE9MjI2dd`
			}
		})
	]),
	footer({state, actions})
]);
