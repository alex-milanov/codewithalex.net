// dom
const {
	h1, a, div, header, iframe,
	section, button, span, video, source
} = require('iblokz-snabbdom-helpers');
// components
const Hls = require('hls.js');
const videoJs = require('video.js').default;

console.log('video.js', videoJs);

// lib
const Rx = require('rx');
const $ = Rx.Observable;

const url = 'https://stream.codewithalex.lan/hls/0/stream.m3u8';
console.log(url);

module.exports = ({state, actions}) => div('.video-container', {
	style: {
		left: `${state.offline || state.video ? 240 : 0}px`,
		width: `calc(100% - ${(state.offline || state.video ? 240 : 0) + (state.chat ? 420 : 0)}px)`
	}
}, [
	div('.video-info', [
		state.videoId === -1
			? state.offline
				? `Stream is offline, please come back later or check some of the previous streams`
				: `Currently watching Livestream, view count: ${state.viewCount}`
			: state.videos[state.videoId].title
	]),
	(state.videoId > -1 || state.offline === false)
		? video({hook: {
			insert: ({elm}) => {
				let vid = videoJs(elm, {
					autoplay: true,
					controls: true,
					liveui: true
				});
				vid.addClass('video-js');
				vid.addClass('vjs-default-skin');

				// vid.on('error', () => state.videoId === -1 ? actions.set('offline', true) : true);
				// vid.play();
				// if (Hls.isSupported()) {
				// 	var hls = new Hls();
				// 	hls.loadSource(url);
				// 	hls.attachMedia(elm);
				// 	hls.on(Hls.Events.MANIFEST_PARSED, function() {
				// 		elm.play();
				// 	});
				// } else if (video.canPlayType('application/vnd.apple.mpegurl')) {
				// 	elm.src = url;
				// 	elm.addEventListener('loadedmetadata', function() {
				// 		elm.play();
				// 	});
				// }
			}
		}}, [
			source({
				hook: {
					update: ({elm}) => {
						let vid = videoJs(elm.parentNode);
						// console.log(
						// 	elm.getAttribute('src'),
						// 	vid.currentSrc()
						// );
						if (vid.currentSrc() !== elm.getAttribute('src')) {
							vid.src({
								src: elm.getAttribute('src'),
								type: elm.getAttribute('type')
							});
							// vid.play();
						}
					}
				},
				attrs: {
					src: state.videoId === -1
						? url
						: state.videos[state.videoId].url,
					type: state.videoId === -1 ? 'application/x-mpegURL' : state.videos[state.videoId].type
				}})
		])
	: ''
]);
