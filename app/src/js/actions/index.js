'use strict';

const {obj, arr} = require('iblokz-data');

// namespaces=
const counter = require('./counter');

// initial
const initial = {
	chat: false,
	video: false,
	videoId: -1,
	offline: true,
	viewCount: 0,
	videos: [
		{
			title: `Code with Alex 2020-04-10 Initial Stream Connection`,
			dateTime: `2020-04-10T19:45`,
			url: `/videos/codewithalex-20200410-194306.mp4`,
			type: `video/mp4`,
			thumb: `/videos/codewithalex-20200410-194306.png`
		},
		{
			title: `Code with Alex 2020-04-11 video.js integration, midi-pipes refactoring and ui improvements`,
			dateTime: `2020-04-11T18:00`,
			url: `/videos/codewithalex-20200411-172555.mp4`,
			type: `video/mp4`,
			thumb: `/videos/codewithalex-20200411-172555.png`
		},
		{
			title: `Code with Alex 2020-04-12 added sync scripts, previous streamed video playing support`,
			dateTime: `2020-04-12T15:00`,
			url: `/videos/codewithalex-20200412-150756.mp4`,
			type: `video/mp4`,
			thumb: `/videos/codewithalex-20200412-150756.png`
		},
		{
			title: `Code with Alex 2020-06-14 Tinkerday Livecoding from init Lab (Arduino MIDI Sequencer)`,
			dateTime: `2020-06-14T16:00`,
			url: `/videos/codewithalex-20200614-160208.mp4`,
			type: `video/mp4`,
			thumb: `/videos/codewithalex-20200614-160208.png`
		}
	]
};

// actions
const set = (key, value) => state => obj.patch(state, key, value);
const toggle = key => state => obj.patch(state, key, !obj.sub(state, key));
const arrToggle = (key, value) => state =>
	obj.patch(state, key,
		arr.toggle(obj.sub(state, key), value)
	);

const changeVideo = (videoId = -1) => state => Object.assign({}, state, {
	videoId
});

const setStatus = ({viewerCount = 0, online}) => state => Object.assign({}, state, {
	viewCount: viewerCount,
	offline: !online
});

module.exports = {
	initial,
	counter,
	set,
	toggle,
	arrToggle,
	changeVideo,
	setStatus
};
