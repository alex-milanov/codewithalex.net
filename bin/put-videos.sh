#!/bin/bash
export $(grep -v '^#' .env | xargs)
rsync -avuz -P $VIDEOS_PATH{*.mp4,*.png} $VOD_SERVER:$VOD_VIDEOS_PATH
