#!/bin/bash
export $(grep -v '^#' .env | xargs)
for a in $VIDEOS_PATH*.flv; do
  b="${a[@]/%flv/mp4}"
	c="${a[@]/%flv/png}"
	echo "$a"
	if [ ! -f $b ]; then
		echo "$b"
		ffmpeg -analyzeduration 50000000 -v verbose -y -i "$a" -vcodec libx264 -crf 28 "$b"
	fi
	if [ ! -f $c ]; then
		echo "$c"
		ffmpeg -i $a -vf "select=eq(n\,155)" -q:v 3 -vframes 1 -vcodec png "$c"
	fi
done

#  ffmpeg -i input.mp4 -vcodec libx264 -crf 28 output.mp4
