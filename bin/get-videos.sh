#!/bin/bash
export $(grep -v '^#' .env | xargs)
rsync -avuz -P $STRM_SERVER:$STRM_DUMP_PATH $VIDEOS_PATH
